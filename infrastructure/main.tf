terraform {
  required_version = ">= 0.12, < 0.13"
}

module "cluster" {
  source  = "./cluster"
  project = var.project
  region  = var.region
  zone    = var.zone

  environment = var.environment
}

provider "google" {
  version = "~> 2.3"
  project = var.project
  region  = var.region
}
