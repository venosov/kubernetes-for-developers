output "cluster-ip-address" {
  value       = module.cluster.ip-address
  description = "Load balancer IP address"
}
