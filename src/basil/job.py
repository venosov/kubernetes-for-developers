import asyncio
import json
import random
import time

import aioredis
from structlog import get_logger

from basil.config import EVENTS, REDIS_URI, logging_setup
from basil.models import Payload

logging_setup()

logger = get_logger("basil.job")


async def background_job():
    past = time.time()

    redis = await aioredis.create_redis(REDIS_URI)

    payload = Payload(
        timestamp=time.time(),
        sensor="temperature",
        data={"value": random.randint(-50, 100)},
    )

    logger.info("Adding new event", payload=payload)

    await redis.lpush(EVENTS, json.dumps(payload.dict()))

    await asyncio.sleep(random.randint(1, 5))

    redis.close()
    await redis.wait_closed()

    runtime = time.time() - past
    logger.info("Job completed in %s seconds", runtime)


if __name__ == "__main__":
    asyncio.run(background_job())
