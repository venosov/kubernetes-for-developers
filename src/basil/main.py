import json
from typing import Dict

import aioredis
from fastapi import FastAPI
from structlog import get_logger

from basil.config import CONTAINER_NAME, EVENTS, REDIS_URI, logging_setup
from basil.models import Payload

logging_setup()

logger = get_logger()
app = FastAPI()


async def get_events(start: int = 0, stop: int = 100) -> list:
    events = await app.redis.lrange(EVENTS, start, stop)
    return list(map(json.loads, events))


async def append_event(payload: Payload) -> Dict:
    json_dump = json.dumps(payload.dict())
    await app.redis.lpush(EVENTS, json_dump)
    return json_dump


@app.on_event("startup")
async def startup_event():
    app.redis = await aioredis.create_redis(REDIS_URI)
    logger.info("Connected to redis")


@app.on_event("shutdown")
async def shutdown_event():
    logger.info("Closing redis connection")
    app.redis.close()
    await app.redis.wait_closed()


@app.get("/events")
async def get_endpoint(start: int = 0, stop: int = 100):
    """Return a list of events"""
    events = await get_events(start, stop)
    return {"events": events}


@app.post("/event")
async def set_endpoint(payload: Payload):
    """Create a new event"""
    logger.info("Creating a new event", payload=payload)
    json_dump = await append_event(payload)
    return {"status": "created", "payload": json_dump}


@app.get("/healthcheck")
async def healthcheck_endpoint():
    """Health endpoint

    Returns 200, status=OK and container_name
    """
    STATUS = "OK"
    logger.info(
        "Healthcheck endpoint checked", status=STATUS, container_name=CONTAINER_NAME
    )
    return {"status": STATUS, "container": CONTAINER_NAME}
