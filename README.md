# Kubernetes 101 for Python Developers

![img](background.png)

## Installation notes and requirements

This tutorial will require recent installations of:

- [docker](https://hub.docker.com/search/?type=edition&offering=community)
- [docker-compose](https://docs.docker.com/compose/install/)
- [gcloud SDK](https://cloud.google.com/sdk/)
- [Terraform 0.12.x](https://www.terraform.io/downloads.html)
- [kubectl v1.13.x](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Python 3.7](https://www.python.org/downloads/)
- [Poetry](https://github.com/sdispater/poetry)

A [GCP account](https://console.cloud.google.com) **IS REQUIRED**.

The account will be used to create and run our GKE instance (and yes it requires to have a valid billing account with a credit card).

To run the application locally

```
python3.7 -m venv .venv
source .venv/bin/activate
poetry install
```

To start the application

```
uvicorn src.basil.main:app --reload
```

# Setting up gcloud

[Before create a new project inside your GCP account](https://cloud.google.com/resource-manager/docs/creating-managing-projects).

After you have created your project you need to update the `project` variable inside `infrastructure/terraform.tfvars` using your `project id` (not the `project name`!).

First you need to authenticate your CLI

```
gcloud auth login
```

Now you can connect initialize gcloud and select the project you have just created

```
gcloud init
```

Great, time to activate the APIs we need

```
gcloud services enable compute.googleapis.com container.googleapis.com
```

and treate a local auth key which is available to terraform as well.

```
gcloud auth application-default login
```

Now you are ready to go!

# Slides

Slides are available inside the [/slides](/slides) folder.
