# Kubernetes

--

### Kubernetes

> Kubernetes is a portable, extensible, open-source platform
> for managing containerized workloads and services,
> that facilitates both declarative configuration and automation.


In other words, Kubernetes is a `container management system`.

--

### What can you do with Kubernetes?

- create a cluster (group of VMs)
- deploy the basil app
- run cronjobs
- define the number of running containers
- add/remove new nodes _seamlessly_
- collect logs and metrics
- Be happy 😎 (until something goes bad...)

--

Kubernetes is farly complex.

<img src="static/k8s-arch.png" width="90%">

[Credit](https://medium.com/containermind/a-reference-architecture-for-deploying-wso2-middleware-on-kubernetes-d4dee7601e8e)

--

### Nodes

You have master nodes and.... just nodes.

```text
The “master” refers to a collection of processes
managing the cluster state (master components).

Typically all these processes
run on a single node in the cluster,
and this node is also referred to as the master.

The master can also be replicated
for availability and redundancy.
```

--

You are free to run any kind of workload on the master node/s.

But in case of a managed service (i.e. GKE, EKS, AKS) __you don't operate the master node/s__.

--

### Components

In kubernetes you have multiple components.

As we said before we will consider as:

- the `master components` are running on a `master node`
- the `node components` the ones running or `nodes`

--

### Master components

- kube-apiserver
- kube-scheduler
- kube-controller-manager
- etcd
- cloud-controller-manager (specific to the cloud providers)

--

### Node components

- kubelet
- kube-proxy
- Container Runtime (Docker, rkt, ...)

[More info about Kubernetes components](https://kubernetes.io/docs/concepts/overview/components/)

--

### Kubernetes objects

Kubernetes objects are resources, entities inside your Kubernetes system.

Example:

- nodes
- pods
- secrets
- volumes
- deployments
- configurations

--

When you define a Kubernetes object you declare the desired state and Kubernetes will constantly
work to maintain that state.

--

### A kubernetes object

```yaml
apiVersion: apps/v1 #
kind: Deployment #
metadata: #
  name: nginx-deployment
spec: #
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

You can check the `spec` format for `Deployment` [here](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.15/#deployment-v1-apps).

--

### Managing kubernetes objects

To manage objects inside our kubernetes cluster we communicate with the `kube-apiserver`.

We can use the [Kubernetes API](https://kubernetes.io/docs/reference/) or `kubectl`.

There are 2 ways of managing objects inside Kubernetes:

- imperative
- declarative

We will see the difference later.

--

### How to run kubernetes

There are multiple ways to run Kubernetes

- Docker on OS X and Windows
- [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)
- [Microk8s](https://microk8s.io/)
- [kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)
- [kops](https://kubernetes.io/docs/setup/production-environment/tools/kops/)
- Managed solutions
- .....

--

<img src="static/kubernetessolutions.svg" width="90%">

[Credit](https://kubernetes.io/docs/setup/#production-environment)

--

Today we will run on Kubernetes, managed by Google, [Kubernetes Engine](https://cloud.google.com/kubernetes-engine/).

--

# Q&A Session
