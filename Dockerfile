FROM python:3.7.4-alpine as base
ENV PATH /home/user/.venv/bin:/home/user/.poetry/bin:$PATH
ENV PYTHONPATH /home/user
RUN adduser -S user
RUN apk update && \
    apk add --no-cache --virtual .build-deps \
    build-base \
    curl \
    git && \
    apk add --no-cache tini
USER user
WORKDIR /home/user
RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
COPY poetry.lock* pyproject.toml /home/user/
RUN poetry config settings.virtualenvs.in-project true && \
    poetry install --no-dev
COPY . /home/user
ENTRYPOINT ["/sbin/tini", "--"]

FROM base as production
USER root
RUN apk del .build-deps
USER user
WORKDIR /home/user/src
CMD ["uvicorn", "basil.main:app", "--host", "0.0.0.0", "--port", "8080"]


FROM base as tests
USER root
RUN poetry install
RUN apk del .build-deps